# translation of powerdevilactivitiesconfig.po to Khmer
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilactivitiesconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-06 02:04+0000\n"
"PO-Revision-Date: 2012-06-14 14:12+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ខឹម សុខែម, ម៉ន ម៉េត, សេង សុត្ថា, ចាន់ សម្បត្តិរតនៈ, សុខ សុភា"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"khoemsokhem@khmeros.info,​​mornmet@khmeros.info,sutha@khmeros.info,"
"ratanak@khmeros.info,sophea@khmeros.info"

#: activitypage.cpp:59
#, kde-format
msgid ""
"The activity service is running with bare functionalities.\n"
"Names and icons of the activities might not be available."
msgstr ""
"សេវាកម្ម​សកម្មភាព​កំពុង​ដំណើរការ​ជា​មួយ​​មុខងារ​ហាមឃាត់ ។\n"
"ឈ្មោះ និង​រូបតំណាង​របស់​សកម្មភាព​អាច​ប្រើ​មិន​បាន ។"

#: activitypage.cpp:132
#, kde-format
msgid ""
"The activity service is not running.\n"
"It is necessary to have the activity manager running to configure activity-"
"specific power management behavior."
msgstr ""
"សេវាកម្ម​សកម្មភាព​មិន​កំពុង​ដំណើរការ​ទេ ។\n"
"វាចាំបាច់​ត្រូវ​ឲ្យ​កម្មវិធី​គ្រប់គ្រង​សកម្មភាព​ដំណើរការ ដើម្បី​កំណត់​រចនាសម្ព័ន្ធ​ឥរិយាបថ​គ្រប់គ្រង​ថាមពល​"
"ជាក់លាក់ ។"

#: activitypage.cpp:227
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, noSettingsRadio)
#: activityWidget.ui:19
#, fuzzy, kde-format
#| msgid "Don't use special settings"
msgid "Do not use special settings"
msgstr "កុំ​ប្រើ​ការ​កំណត់​ពិសេស"

#. i18n: ectx: property (text), widget (QRadioButton, specialBehaviorRadio)
#: activityWidget.ui:29
#, kde-format
msgid "Define a special behavior"
msgstr "កំណត់​ឥរិយាបថ​ពិសេស"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownScreenBox)
#: activityWidget.ui:41
#, fuzzy, kde-format
#| msgid "Never shutdown the screen"
msgid "Never turn off the screen"
msgstr "កុំ​បិទ​អេក្រង់"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownPCBox)
#: activityWidget.ui:48
#, fuzzy, kde-format
#| msgid "Never shutdown the screen"
msgid "Never shut down the computer or let it go to sleep"
msgstr "កុំ​បិទ​អេក្រង់"

#, fuzzy
#~| msgid "Sleep"
#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "ដេក"

#~ msgid "Hibernate"
#~ msgstr "សង្ងំ"

#, fuzzy
#~| msgid "Shutdown"
#~ msgid "Shut down"
#~ msgstr "បិទ"

#~ msgid "Always"
#~ msgstr "ជា​និច្ច"

#~ msgid "after"
#~ msgstr "បន្ទាប់ពី"

#~ msgid " min"
#~ msgstr " នាទី"

#~ msgid "PC running on AC power"
#~ msgstr "កុំព្យូទ័រ​ដំណើរការ​ដោយ​ប្រើ​ថាមពល AC"

#~ msgid "PC running on battery power"
#~ msgstr "កុំព្យូទ័រ​ដំណើរការ​ដោយ​ប្រើថ្ម"

#~ msgid "PC running on low battery"
#~ msgstr "កុំព្យូទ័រ​ដំណើរការ​ដោយ​ប្រើថ្ម​ខ្សោយ"

#~ msgctxt "This is meant to be: Act like activity %1"
#~ msgid "Activity \"%1\""
#~ msgstr "សកម្មភាព \"%1\""

#~ msgid "Act like"
#~ msgstr "ធ្វើសកម្មភាព​ដូច"

#, fuzzy
#~| msgid "Use separate settings (advanced users only)"
#~ msgid "Use separate settings"
#~ msgstr "ប្រើ​ការ​កំណត់​ដោយ​ឡែក (សម្រាប់​តែ​អ្នក​ប្រើ​កម្រិត​ខ្ពស់)"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "សេវាកម្ម​គ្រប់គ្រង​ថាមពល​បង្ហាញ​ថា​មិន​កំពុង​ដំណើរការ​ទេ ។\n"
#~ "វា​អាច​ត្រូវ​បានដោះស្រាយ​ដោយ​ចាប់ផ្ដើម ឬ​កំណត់​កាលវិភាគ​ឡើងវិញ នៅ​ក្នុង \"ចាប់ផ្ដើម និង​បិទ\""

#~ msgid "Never suspend or shutdown the computer"
#~ msgstr "កុំ​ផ្អាក ឬ​បិទកុំព្យូទ័រ"

#~ msgid "Activities Power Management Configuration"
#~ msgstr "ការកំណត់​រចនាសម្ព័ន្ធ​ការ​គ្រប់គ្រង​ថាមពល​សកម្មភាព"

#~ msgid "A per-activity configurator of KDE Power Management System"
#~ msgstr "កម្មវិធី​កំណត់​រចនាសម្ព័ន្ធ​ក្នុង​មួយ​សកម្ម​នៃ​ប្រព័ន្ធ​គ្រប់គ្រង​ថាមពល​របស់ KDE"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "រក្សាសិទ្ធិ​ឆ្នាំ ២០១០ ដោយ Dario Freddi"

#~ msgid ""
#~ "From this module, you can fine tune power management settings for each of "
#~ "your activities."
#~ msgstr "ពី​ម៉ូឌុល​នេះ អ្នក​អាច​​កែសម្រួល​កា​រកំណត់​ការ​គ្រប់គ្រង​ថាមពល​សម្រាប់​សកម្មភាព​នីមួយៗ​របស់​អ្នក ។"

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "អ្នក​ថែទាំ"
